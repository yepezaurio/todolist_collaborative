import React, { useState, useEffect } from "react";
import { TaskRow } from "./components/TaskRow";
import { TaskBanner } from "./components/TaskBanner";
import { TaskCreator } from "./components/TaskCreator";
import { VisibilityControl } from "./components/VisibilityControl";

function App() {
  //Insert user name
  const [userName, setuserName] = useState("yepez");

  //Array for task
  const [taskItems, settaskItem] = useState([]);

  //For show task completes in the table of down
  const [showCompleted, setshowCompleted] = useState(true);

  /**
   *  Useeffect is for load first in the page.
   *  In this case is configurated for if not exist task in the localStorage
   *  we going to load a task and taskItems ( Before, i deleted itemstask for test, the task can load into [] )
   *
   *  The function "ShowCompleted" is a checkbox in page for show task's completed in other table.
   */
  useEffect(() => {
    let data = localStorage.getItem("task");
    if (data != null) {
      settaskItem(JSON.parse(data));
    } else {
      setuserName("Yepezaurio");
      settaskItem([]);
      setshowCompleted(true);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("task", JSON.stringify(taskItems));
  }, [taskItems]);

  /** <----------- F U N C T I O N S ------------ **/

  // change state task
  const toggleTask = (task) =>
    settaskItem(
      taskItems.map((t) => (t.name === task.name ? { ...t, done: !t.done } : t))
    );

  // map of task and show in the screen
  const taskTableRows = (taskdone) =>
    taskItems
      .filter((task) => task.done === taskdone)
      .map((task) => (
        <TaskRow
          task={task}
          key={task.name}
          toggleTask={toggleTask}
          callback={deleteTask}
          editTask={editTask}
        />
      ));

  // Validate and add task in the list of task
  const createNewTask = (taskName) => {
    // Compare  if we've the same task in the list for not add the same
    if (!taskItems.find((t) => t.name === taskName)) {
      settaskItem([...taskItems, { name: taskName, done: false }]);
    }
  };

  // Delete Task
  const deleteTask = (keyTask) => {
    let remove = taskItems.indexOf(taskItems.find((t) => t.name === keyTask));
    taskItems.splice(remove, 1);
    settaskItem([...taskItems]);
  };

  //Edit Task
  const editTask = (newTask, task) => {
    if (task !== newTask) {
      taskItems.map((t) => (t.name === task ? (t.name = newTask) : task));
      settaskItem([...taskItems]);
    }
  };

  /** <----------- F U N C T I O N S ------------/> **/

  return (
    <div className="App">
      <TaskBanner userName={userName} taskItems={taskItems} />
      <TaskCreator callback={createNewTask} />
      <table className=" table table-striped table-bordered">
        <thead>
          <tr>
            <th>Description</th>
            <th>Done</th>
          </tr>
        </thead>
        <tbody>{taskTableRows(false)}</tbody>
      </table>
      <div className="bg-secondary-text-white text-center p-2">
        <VisibilityControl
          description="Completed task"
          ischecked={showCompleted}
          callback={(checked) => setshowCompleted(checked)}
        />
      </div>
      {showCompleted && (
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Description </th>
              <th>Done</th>
            </tr>
          </thead>
          <tbody>{taskTableRows(true)}</tbody>
        </table>
      )}
    </div>
  );
}

export default App;
