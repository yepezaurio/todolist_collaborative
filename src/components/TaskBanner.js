import React from "react";

//Return to name and number of task for complete in top banner
export const TaskBanner = (props) => (
  <h4 className="bg-primary text-white text-center p-4">
    {props.userName}'s Task app ({props.taskItems.filter((t) => !t.done).length}{" "}
    tasks to do)
  </h4>
);
