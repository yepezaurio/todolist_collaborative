import React, { useState } from "react";

export const TaskCreator = (props) => {
  const [newTaskName, setnewTaskName] = useState("");

  /** -----------M E T H O D S------------*/

  // This variable is altered while the user types ( variable is newTaskName )
  const updateNewTaskValue = (e) => setnewTaskName(e.target.value);

  // Add task of the list and clear input
  const createNewTask = () => {
    props.callback(newTaskName);
    setnewTaskName("");
  };
  /** ------------------------------------*/

  return (
    <div className="my-1">
      <input
        type="text"
        className="form-control"
        value={newTaskName}
        onChange={updateNewTaskValue}
      />
      <button className="btn btn-primary mt-1" onClick={createNewTask}>
        add
      </button>
    </div>
  );
};
