import React, { useState } from "react";
import { MdDelete, MdEdit } from "react-icons/md";

import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

/** <------------ S T Y L E S ------------ **/
const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));
/** ------------ S T Y L E S ------------ /> **/

//Return key of task
export const TaskRow = (props) => {
  /** <------------ F U N C T I O N S ------------ **/

  //Variable used for editTask
  const [editTaskName, seteditTaskName] = useState("");

  //Change value of task
  const updateNewTaskValue = (e) => seteditTaskName(e.target.value);

  // Toggle Modal
  const [open, setOpen] = React.useState(false);

  // Create object with styles created
  const classes = useStyles();

  //Open modal
  const handleOpen = () => {
    seteditTaskName(props.task.name);
    setOpen(true);
  };

  //Close modal
  const handleClose = () => {
    setOpen(false);
  };

  // Delete task.
  const deleteTask = () => {
    props.callback(props.task.name);
  };

  // Edit task ( Output: newTask , olderTask )
  const editTask = () => {
    props.editTask(editTaskName, props.task.name);
    handleClose();
  };
  /** ------------ F U N C T I O N S  ------------/> **/
  return (
    <tr key={props.task.name}>
      <td>{props.task.name}</td>
      <td>
        <input
          type="checkbox"
          checked={props.task.done}
          onChange={() => props.toggleTask(props.task)}
        />
        <MdDelete color="red" size="1.5em" onClick={deleteTask} />
        <MdEdit color="blue" size="1.5em" onClick={handleOpen} />
      </td>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2>Edit task</h2>
            <input
              type="text"
              className="form-control"
              value={editTaskName}
              onChange={updateNewTaskValue}
            />
            <button className="btn btn-primary mt-1" onClick={editTask}>
              Update
            </button>
          </div>
        </Fade>
      </Modal>
    </tr>
  );
};
